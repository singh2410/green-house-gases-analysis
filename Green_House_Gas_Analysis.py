#!/usr/bin/env python
# coding: utf-8

# # Green House Gas Analysis Model
# #By- Aarush Kumar
# #Dated: June 22,2021

# In[1]:


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import plotly.express as px
import os
get_ipython().run_line_magic('matplotlib', 'inline')


# In[2]:


data = pd.read_csv('/home/aarush100616/Downloads/Projects/Green House Gases Analysis/emission_data.csv')


# In[3]:


data


# In[4]:


data.head()


# In[5]:


data.info()


# In[6]:


data.isnull().sum()


# In[7]:


data.describe()


# In[8]:


data.shape


# In[9]:


data.size


# In[10]:


data.columns


# In[11]:


columns = ['Country', '1997', '1998', '1999', '2000', '2001', '2002', '2003',
       '2004', '2005', '2006', '2007', '2008', '2009', '2010', '2011', '2012',
       '2013', '2014', '2015', '2016', '2017']


# In[12]:


df = data[columns]


# In[13]:


df.columns


# ##  Descriptive Statistics

# In[14]:


desc = df.describe().T
f, ax = plt.subplots(figsize=(15, 8))
sns.heatmap(desc, annot=True, cmap='CMRmap_r', fmt='.00f',
            ax=ax, linewidths=1, cbar=False,
            annot_kws={'size': 14})
plt.xticks(size = 18)
plt.yticks(size = 12, rotation = 0)
plt.ylabel("Variables")
plt.title("Descriptive Statistics", size = 16)
plt.show()


# In[15]:


df.isnull().sum()


# In[16]:


df.info()


# In[17]:


df['total_emission'] = df.sum(axis=1)


# In[18]:


df[['Country','total_emission']].head()


# ## Analysis for top ten economies

# In[19]:


top = df.sort_values(by='total_emission', ascending=False)


# In[20]:


top


# In[21]:


top = top.drop(index=[227,64,72,13,4,138,1], axis=0)


# In[22]:


df_economies = top.head(10)


# In[23]:


brazil = top[top['Country'] == 'Brazil']


# In[24]:


df_economies = pd.concat([df_economies, brazil])


# In[25]:


df_economies.columns


# In[26]:


sns.catplot(x='Country', y='total_emission', data=df_economies, kind='bar', height=5, aspect=25/8, palette='inferno')


# In[27]:


sns.catplot(x='Country', y='total_emission', data=df_economies, height=5, aspect=25/8, palette='inferno')


# In[28]:


fig = plt.figure()
ax = fig.add_axes([0, 0, 2, 2])
ax.axis('equal')
ax.pie(df_economies['total_emission'], labels = df_economies["Country"], autopct='%1.2f%%')
plt.show()


# In[29]:


df_habit = pd.DataFrame()
df_habit = df_economies['total_emission'].iloc[0:2]


# In[30]:


emission_eua = df_habit.iloc[0] / int(326e6)
emission_china = df_habit.iloc[1] / int(1.386e9)


# In[31]:


fig = plt.figure()
ax = fig.add_axes([0, 0, 2, 2])
ax.axis('equal')
ax.pie([emission_eua, emission_china], labels=['EUA', 'CHINA'], autopct='%1.2f%%')
plt.show()


# In[32]:


df_economies


# In[33]:


usa   = df_economies.loc[(df_economies['Country'] == 'United States'), 'total_emission'].iloc[0]
china = df_economies.loc[(df_economies['Country'] == 'China'), 'total_emission'].iloc[0]
india = df_economies.loc[(df_economies['Country'] == 'India'), 'total_emission'].iloc[0]


# In[34]:


fig = plt.figure()
ax = fig.add_axes([0, 0, 2, 2])
ax.axis('equal')
plt.pie([usa, china, india], labels = ['United States', 'China', 'India'], autopct='%1.2f%%')
plt.show()


# In[35]:


df_economies['Country']


# In[36]:


df1 = df_economies.drop('total_emission', axis=1)


# In[37]:


df = df1.T
df = df.reset_index()


# In[38]:


df.columns = df.iloc[0]
df = df.rename(columns = {'Country':'year'})
df = df.drop(0)


# In[39]:


df = df.astype('float')
df['year'] = df['year'].astype('int')


# In[40]:


df.head()


# In[41]:


plt.figure(figsize=(16, 8))
sns.lineplot(x='year', y='United States', data=df, label = 'US')
sns.lineplot(x='year', y='China', data=df, label = 'CH')
sns.lineplot(x='year', y='Russia', data=df, label = 'Russia')
sns.lineplot(x='year', y='Germany', data=df, label = 'GER')
sns.lineplot(x='year', y='United Kingdom', data=df, label = 'UK')
sns.lineplot(x='year', y='France', data=df, label = 'FRA')
sns.lineplot(x='year', y='India', data=df, label = 'IND')
sns.lineplot(x='year', y='Canada', data=df, label = 'CAN')
sns.lineplot(x='year', y='Poland', data=df, label = 'POL')
sns.lineplot(x='year', y='Brazil', data=df, label = 'BRA')
plt.show()


# In[42]:


plt.figure(figsize=(16, 8))
sns.lineplot(x='year', y='United States', data=df, label = 'US')

